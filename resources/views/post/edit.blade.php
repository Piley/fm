@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header lead">Редактировать пост #{{$post->id}}</div>
                    <div class="card-body">
                        <form action="{{ route('post.update', $post->id) }}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">

                            @include('parts.alerts')
                            @csrf
                            <div class="form-group">
                                <input value="{{ $post->title ?? old('title') }}" class="form-control" type="text" name="title" placeholder="Название">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="body" cols="30" rows="5" placeholder="Содержание">{{ $post->body ?? old('body') }}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
