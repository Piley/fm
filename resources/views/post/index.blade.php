@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Посты</h1>
                <p class="lead">
                    Полезная информация и мнения других участников
                </p>
                <div class="row">

                    @foreach ($posts as $post)
                        <div class="col-md-12">
                            <div class="card text-center form-group">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $post->title  }}</h5>
                                    <p class="card-text">{{  Str::limit($post->body, 200) }}</p>
                                    <a href="{{ url('post', $post->id) }}" class="btn btn-dark">Читать полностью</a>
                                </div>
                                <div class="card-footer text-muted">
                                    {{ $post->created_at }}
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
