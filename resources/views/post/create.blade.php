@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header lead">Создать пост</div>
                    <div class="card-body">
                        <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                            @include('parts.alerts')
                            @csrf
                            <div class="form-group">
                                <input value="{{ $post->title ?? old('title') }}" class="form-control" type="text" name="title" placeholder="Название">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="body" cols="30" rows="5" placeholder="Содержание">{{ $post->body ?? old('description') }}</textarea>
                            </div>


                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Категория</label>
                                    </div>
                                    <select name="category_id" class="custom-select">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Создать</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
