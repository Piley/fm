@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header lead">{{ $post->title }}</div>
                    <div class="card-body">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <p>{{ $post->body }}</p>
                            </div>
                        </div>
                    </div>

                </div>

                <hr>
                <div class="card">
                    <div class="card-header lead">
                        <small>Обсуждение</small>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ url('post_add_comment') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="new_comment">Оставить комментарий:</label>
                                        <textarea required name="body" class="form-control" id="new_comment" rows="3"></textarea>
                                        <input type="text" hidden name="post_id" value="{{$post->id}}">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Отправить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr>
                        @foreach ($post->comments as $comment)
                            <div class="row">
                                <div class="col-md-2">
                                    <img class="img-fluid rounded-circle img-thumbnail"
                                         src="@if ($comment->user->image){{url("storage/uploads/images/user/".$comment->user->image)}}@else{{asset('images/noava.png')}}@endif">

                                    <span>Уровень 6</span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                             style="width: 25%" aria-valuenow="25" aria-valuemin="0"
                                             aria-valuemax="100"></div>
                                    </div>
                                    <p>
                                        <span class=" badge badge-pill badge-danger">Неудержимый</span>
                                    </p>
                                </div>

                                <div class="col-md-10">
                                    <p>
                                        <strong><a href="{{ url('user', $comment->user->id) }}">{{ $comment->user->name }}</a></strong>
                                        <span class="float-right">{{ $comment->created_at->format('d.m.Y H:i') }}</span>
                                    </p>
                                    <p>{{ $comment->body }}</p>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
