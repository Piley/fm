@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header lead">Редактировать комментарий</div>

                    <div class="card-body">
                        <form action="{{ route('marathon_edit_comment_update', $comment->id) }}" method="post" enctype="multipart/form-data">
                            @include('parts.alerts')
                            @csrf
                            <div class="form-group">
                                <textarea class="form-control" name="body" cols="30" rows="5" placeholder="Содержание">{{ $comment->body ?? old('body') }}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
