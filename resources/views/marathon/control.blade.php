@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header lead">{{ $marathon->title }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Имя</th>
                                        <th scope="col">Статус</th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($marathon->users as $user)
                                        <tr>
                                            <th scope="row">{{ $user->id }}</th>
                                            <td>{{ $user->name }}</td>
                                            <td>
                                                @if ($user->pivot->status == 1)
                                                    Активный
                                                @elseif ($user->pivot->status == 2)
                                                    Вылетел
                                                @else
                                                    Отменил заявку
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->pivot->status == 1)
                                                    <button class="btn btn-danger btn-sm return-member" data-user_id="{{ $user->id }}">Кикнуть</button>
                                                @elseif ($user->pivot->status == 2)
                                                    <button class="btn btn-secondary btn-sm return-member" data-user_id="{{ $user->id }}">Вернуть</button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        // вернуть участника
        $(document).on('click', '.return-member', function(){
            var user_id = $(this).data('user_id');

            $.ajax({
                type: 'POST',
                url: '{{ route('marathon.kick', $marathon->id) }}',
                data: { user_id: user_id, _token: '{{ csrf_token() }}'},
                success: function(data){
                    location.reload();
                }
            });

        });
    </script>
@endsection
