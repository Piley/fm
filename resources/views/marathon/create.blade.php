@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header lead">Создать марафон</div>

                    <div class="card-body">


                        <form action="{{ route('marathon.store') }}" method="post" enctype="multipart/form-data">
                            @include('parts.alerts')
                            @csrf

                            <div class="form-group">
                                <input value="{{ $marathon->title ?? old('title') }}" class="form-control" type="text" name="title" placeholder="Название">
                            </div>
                            <div class="form-group">
                                <label for="image">Картинка</label>
                                <input class="form-control" type="file" name="image">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="description" cols="30" rows="5" placeholder="Описание">{{ $marathon->description ?? old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="rules" cols="30" rows="5" placeholder="Правила">{{ $marathon->rules ?? old('rules') }}</textarea>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="tips" cols="30" rows="5" placeholder="Полезная информация">{{ $marathon->tips ?? old('tips') }}</textarea>
                            </div>
                            <div class="form-group">
                                <input value="{{ $marathon->auto_kick_days ?? old('auto_kick_days') }}" class="form-control" type="number" name="auto_kick_days" placeholder="Автокик через n-дней" min="0">
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Тип марафона</label>
                                    </div>
                                    <select name="repeat_type" class="custom-select">
                                        <option selected value="0">Ежедневный</option>
                                        <option value="1">Дней в неделю</option>
                                        <option value="2">Специальный</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <input value="{{ $marathon->weekends ?? old('weekends') }}" class="form-control" type="number" name="weekends" placeholder="Кол-во выходных дней" min="0">
                            </div>
                            <div class='form-group'>
                                <label>Дата начала марафона</label>
                                <input value="{{ $marathon->start_at ?? old('start_at') }}" class="form-control" type="date" name="start_at">
                            </div>
                            <div class='form-group'>
                                <label>Дата окончания марафона</label>
                                <input value="{{ $marathon->end_at ?? old('end_at') }}" class="form-control" type="date" name="end_at">
                            </div>
                            <div class='form-group'>
                                <label>Дата начала регистрации</label>
                                <input value="{{ $marathon->reg_start_at ?? old('reg_start_at') }}" class="form-control" type="date" name="reg_start_at">
                            </div>
                            <div class='form-group'>
                                <label>Дата окончания регистрации</label>
                                <input value="{{ $marathon->reg_end_at ?? old('reg_end_at') }}" class="form-control" type="date" name="reg_end_at">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Создать</button>
                            </div>

                        </form>




                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
