@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header lead">Редактировать марафон #{{$marathon->id}}</div>
                    <div class="card-body">
                        <form action="{{ route('marathon.update', $marathon->id) }}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">

                            @include('parts.alerts')
                            @csrf
                            <div class="form-group">
                                <input value="{{ $marathon->title ?? old('title') }}" class="form-control" type="text" name="title" placeholder="Название">
                            </div>
                            <div class="form-group">
                                <label for="image">Картинка</label>
                                <input class="form-control" type="file" name="image">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="description" cols="30" rows="5" placeholder="Описание">{{ $marathon->description ?? old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="rules" cols="30" rows="5" placeholder="Правила">{{ $marathon->rules ?? old('rules') }}</textarea>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="tips" cols="30" rows="5" placeholder="Полезная информация">{{ $marathon->tips ?? old('tips') }}</textarea>
                            </div>
                            <div class="form-group">
                                <input value="{{ $marathon->auto_kick_days ?? old('auto_kick_days') }}" class="form-control" type="number" name="auto_kick_days" placeholder="Автокик через n-дней" min="0">
                            </div>

                            <div class="form-group">
                                <input value="{{ $marathon->weekends ?? old('weekends') }}" class="form-control" type="number" name="weekends" placeholder="Кол-во выходных дней" min="0">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
