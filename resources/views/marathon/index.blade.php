@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Марафоны</h1>
            </div>
            <div class="col-md-12">
                <p class="lead">
                    Записывайся на марафоны и приобретай новые полезные привычки вместе с другими участниками!
                </p>
            </div>
        </div>

        <div class="row">
            @foreach ($marathons as $marathon)
            <div class="col-md-4 mb-3 marathon-item">
                <div class="card">
                    <div class="marathon-days-badge"><div class="marathon-days-badge-count">{{ $marathon->getDurationDays() }}</div>дней</div>
                    <img class="card-img-top" src="{{url("storage/uploads/images/marathon/".$marathon->image)}}">
                    <div class="card-body">
                        <h5 class="card-title text-center"><a href="{{ route('marathon.show', $marathon->id) }}">{{ $marathon->title }}</a></h5>
                        <p class="card-text text-truncate text-center">{{ $marathon->description }}</p>
                    </div>
                    <div class="marathon-status-badge"><span class="badge">{{ $marathon->getStatusName() }}</span></div>
                </div>
            </div>
            @endforeach

        </div>

       {{ $marathons->render() }}
    </div>
@endsection
