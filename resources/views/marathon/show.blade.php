@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                @if ($marks and $marathon->isUserMember(Auth::id()))
                    <div class="marathon-milestone-wrapper pb-3">
                        @foreach ($marks as $mark)
                            <div class="marathon-milestone-item">
                                <span class="marathon-milestone-point @if($mark['success']) marathon-milestone-point-success @endif @if($mark['today']) marathon-milestone-point-today @endif">{{ $mark['num'] }}</span>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-9">
                                <h1 class="card-title">{{ $marathon->title }}</h1>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            @if (Auth::check())
                                                @if ($status == $marathon::MARATHON_STATUS_PENDING)
                                                    <button disabled class="btn btn-block btn-default">Запись скоро
                                                        начнется
                                                    </button>
                                                @elseif ($status == $marathon::MARATHON_STATUS_REG)
                                                    @if ($marathon->isUserMember(Auth::id()))
                                                        <a href="{{ route('marathon.join', $marathon->id) }}"
                                                           id="marathon_reg_cancel" class="btn btn-block btn-secondary">Отменить
                                                            запись</a>
                                                    @else
                                                        <a href="{{ route('marathon.join', $marathon->id) }}"
                                                           id="marathon_reg"
                                                           class="btn btn-block btn-success">Записаться</a>
                                                    @endif
                                                @elseif ($status == $marathon::MARATHON_STATUS_ACTIVE)
                                                    @if ($marathon->isUserMember(Auth::id()))
                                                        @if ($marathon->isMarkedToday(Auth::id()))
                                                            <button disabled id="marathon_marked_day"
                                                                    class="btn btn-block btn-default">
                                                                Вы отметились за сегодня
                                                            </button>
                                                        @else
                                                            <a href="{{ route('marathon.markDay', $marathon->id) }}"
                                                               id="marathon_marked_day"
                                                               class="btn btn-block btn-primary">Отметиться</a>
                                                        @endif
                                                    @else
                                                        <button disabled class="btn btn-block btn-default">Марафон
                                                            уже начался
                                                        </button>
                                                    @endif

                                                @elseif ($status == $marathon::MARATHON_STATUS_FINISH)
                                                    <button disabled class="btn btn-block btn-default">Марафон
                                                        завершен
                                                    </button>
                                                @endif
                                            @else
                                                <a href="{{ url('login') }}" class="btn btn-block btn-primary">Войдите,
                                                    чтобы
                                                    участвовать</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <p>{!! html_entity_decode($marathon->description) !!}</p>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <p class="lead">Правила</p>
                                    <p>{!! html_entity_decode($marathon->rules ) !!}</p>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <p class="lead">Дополнительно</p>
                                    <p>{!! html_entity_decode($marathon->tips ) !!}</p>
                                </div>

                            </div>

                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{url("storage/uploads/images/marathon/".$marathon->image)}}">
                                <hr>
                                <div class="text-center form-group marathon-members-wrapper">

                                    <img src="/images/avatar.png" width="32">

                                    <div class="marathon-members-badge">
                                        <div class="marathon-members-badge-count">{{ count($marathon->activeUsers) }}</div>
                                        <div class="marathon-members-badge-extra">участников</div>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <div class="card">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">Начало
                                                регистрации: {{ $marathon->reg_start_at->format('d.m.Y') }}</li>
                                            <li class="list-group-item">Окончание
                                                регистрации: {{ $marathon->reg_end_at->format('d.m.Y') }}</li>
                                            <li class="list-group-item">
                                                Старт: {{ $marathon->start_at->format('d.m.Y') }}</li>
                                            <li class="list-group-item">
                                                Финиш: {{ $marathon->end_at->format('d.m.Y') }}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <br>
                <div class="card">
                    <div class="card-header lead">
                        <small>Обсуждение</small>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ url('marathon_add_comment') }}" method="post"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="new_comment">Оставить комментарий:</label>
                                        <textarea required name="body" class="form-control" id="new_comment"
                                                  rows="3"></textarea>
                                        <input type="text" hidden name="marathon_id" value="{{$marathon->id}}">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Отправить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr>
                        {{ $comments->render() }}
                        @foreach ($comments as $comment)
                            <div class="row">
                                <div class="col-2">
                                    <img class="rounded img-thumbnail user-comment-avatar "
                                         src="@if ($comment->user->image){{url("storage/uploads/images/user/".$comment->user->image)}}@else{{asset('images/noava.png')}}@endif">
                                </div>

                                <div class="col-10">
                                    <p>

                                        <strong><a href="{{ url('user', $comment->user->id) }}">{{ $comment->user->name }}</a></strong>
                                        <span class="float-right">{{ $comment->created_at->timezone('Europe/Moscow')->format('d.m.Y H:i') }}</span>
                                    </p>
                                    <p>{!! nl2br(e($comment->body )) !!}</p>
                                    @if (Auth::check())
                                        @if ($comment->user_id == Auth::id() or Auth::user()->isModer())
                                            <p class="float-right">
                                                <a href="{{ route('marathon_edit_comment', $comment->id) }}" class="btn btn-outline-secondary btn-sm"><i class="fas fa-edit"></i>
                                                </a>
                                            </p>
                                        @endif
                                    @endif

                                </div>
                            </div>
                            <hr>
                        @endforeach

                        {{ $comments->render() }}
                    </div>
                </div>


            </div>

        </div>

    </div>
@endsection
