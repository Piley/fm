<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/logo_32.png" type="image/x-icon">

    <title>From Monday - марафоны полезных привычек</title>

    <script src="//code.jivosite.com/widget.js" jv-id="rEAhei5avE" async></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Rubik+Mono+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Russo+One&display=swap" rel="stylesheet">


    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #fff;
            font-weight: 200;
            height: 100vh;
            margin: 0;
            font-family: 'Russo One', sans-serif;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
            background-image: url("/images/bg.png");
            background-repeat:no-repeat;
            background-position:center center;
            background-size:cover;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            font-family: 'Russo One', sans-serif;
            text-align: center;
            color: #fff;
            text-shadow: 1px 1px 2px rgba(0,0,0,0.4), 0 0 1em rgba(0,0,0,0.4); /* Параметры тени */
        }

        .title {
            font-size: 84px;
            font-family: 'Russo One', sans-serif;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 1em;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
            color: #fff;
            font-family: 'Russo One', sans-serif;
        }

        .lead {
            color: #eeeeee;
            font-size: 2em;
        }

        hr {
            border: none;
            color: #fff;
            background-color: rgba(255,255,255,0.5);
            height: 2px;
        }
    </style>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content">
        <hr>
        <div class="title m-b-md">
            From Monday
        </div>
            <p class="lead">Участвуй в марафонах полезных привычек,
                <br>обменивайся опытом,
                <br>узнавай новое</p>
            <p><a href="{{ route('register') }}" class="btn btn-lg btn-primary">Присоединиться</a></p>
        <hr>
        <div class="links">
            <a href="{{ route('marathon.index') }}">Марафоны</a>
{{--            <a href="{{ route('post.index') }}">Посты</a>--}}
            <a href="{{ route('user.index') }}">Люди</a>
            <a target="_blank" href="https://vk.com/frommondayru">Вконтакте</a>
        </div>
    </div>
</div>
@include('parts.yandex')
</body>
</html>
