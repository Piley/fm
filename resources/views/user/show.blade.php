@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    @if ($user->image)
                                        <img class="card-img-top"
                                             src="{{url("storage/uploads/images/user/".$user->image)}}">
                                    @else
                                        <img class="card-img-top"
                                             src="{{url("images/noava.png")}}">
                                    @endif
                                </div>

                                <div class="form-group">
                                    <h4 class="text-center">{{ $user->name }}</h4>
                                </div>
                                <div class="form-group">
                                    <p class="text-center">{{ $user->about }}</p>
                                </div>
                                <div class="form-group">
                                    @if (Auth::check() && Auth::user()->id == $user->id)
                                        <p><a href="{{ url('settings') }}" class="btn btn-dark btn-block">Настройки</a>
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <h3>
                                        <small class="text-muted">Медали</small>
                                    </h3>
                                    @if ($user->medals->count() > 0)
                                        <div class="medal-wrapper">
                                            @foreach ($user->medals as $medal)
                                                <div class="medal-item">
                                                    <img class="img-thumbnail"
                                                         src="{{url("storage/uploads/images/medal/".$medal->image)}}"
                                                         alt="">
                                                    <span class="tooltip" data-tooltip-content="#tooltip_content"
                                                          data-title="{{ $medal->title }}"
                                                          data-description="{{ $medal->description }}"></span>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <h5>
                                            <small class="text-muted">У пользователя еще нет медалей</small>
                                        </h5>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <h3>
                                        <small class="text-muted">Марафоны</small>
                                    </h3>
                                    @if ($user->marathons->count() > 0)
                                        <div class="row">
                                            @foreach ($user->activeMarathons as $marathon)
                                                <div class="col-md-4 mb-3 marathon-item">
                                                    <div class="card">
                                                        <img class="card-img-top"
                                                             src="{{url("storage/uploads/images/marathon/".$marathon->image)}}">
                                                        <div class="card-body">
                                                            <h5 class="card-title"><a
                                                                        href="{{ route('marathon.show', $marathon->id) }}">{{ $marathon->title }}</a>
                                                            </h5>
                                                            <p class="card-text text-truncate">{{ $marathon->description }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <h5>
                                            <small class="text-muted">Пользователь еще не участвовал в марафонах</small>
                                        </h5>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tooltip_templates">
        <div id="tooltip_content">
            <div class="tooltip-head"></div>
            <div class="tooltip-body"></div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var tooltip_content = '';
            $('.tooltip').tooltipster({
                animation: 'grow',
                animationDuration: 200,
                delay: 0,
                contentAsHTML: true,
                functionFormat: function (instance, helper, content) {
                    return tooltip_content
                }
            });
            $.tooltipster.on('start', function (event) {
                var title = $(event.instance.elementOrigin()).data('title')
                var description = $(event.instance.elementOrigin()).data('description')
                tooltip_content = "<div class='tooltip-head'>" + title + "</div><div class='tooltip-body'>" + description + "</div>"
            });
        });
    </script>
@endsection
