@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>Люди</h1>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>
                                                @if ($user->image)
                                                    <img class="user-item-image"
                                                         src="{{url("storage/uploads/images/user/".$user->image)}}">
                                                @else
                                                    <img class="user-item-image"
                                                         src="{{url("images/noava.png")}}">
                                                @endif
                                            </td>
                                            <td>
                                                <a class="lead" href="{{ route('user.show', $user->id) }}">{{ $user->name }}</a>
                                                <p>{{ $user->about }}</p>
                                            </td>
                                            <td class="text-center">
                                                <p class="user-item-count">{{ $user->marathons->count() }}</p>
                                                марафоны
                                            </td>
                                            <td>
                                                <p class="user-item-count">{{ $user->medals->count() }}</p>
                                                медали
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
