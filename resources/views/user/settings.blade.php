@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header lead">Редактирование профиля</div>

                    <form action="{{ url('settings/update') }}" method="post" enctype="multipart/form-data">


                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <img class="img-fluid img-thumbnail"
                                         src="@if ($user->image){{url("storage/uploads/images/user/".$user->image)}}@else{{asset('images/noava.png')}}@endif">

                                    <div class="form-group">
                                        <input class="form-control" type="file" name="image">
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <input type="hidden" name="_method" value="PUT">

                                    @include('parts.alerts')
                                    @csrf

                                    <div class="form-group">
                                        <input value="{{ $user->name ?? old('name') }}" class="form-control" type="text"
                                               name="name" placeholder="Иван Иванов">
                                    </div>
                                    <div class="form-group">
                                        <input value="{{ $user->email ?? old('email') }}" class="form-control"
                                               type="text" name="email" placeholder="example@mail.com">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="about" cols="30" rows="5"
                                                  placeholder="Расскажите о себе">{{ $user->about ?? old('about') }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-secondary btn-sm @if ($user->sex === 0) active @endif">
                                                <input type="radio" name="sex" autocomplete="off" value="0" @if ($user->sex == 0) checked @endif>
                                                Мужской
                                            </label>
                                            <label class="btn btn-secondary btn-sm @if ($user->sex === 1) active @endif">
                                                <input type="radio" name="sex" autocomplete="off" value="1" @if ($user->sex == 1) checked @endif>
                                                Женский
                                            </label>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Сохранить</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
