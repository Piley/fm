@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header lead">Выдать медаль</div>
                    <div class="card-body">
                        <form action="{{ route('medalStore') }}" method="post" enctype="multipart/form-data">
                            @include('parts.alerts')
                            @csrf
                            <div class="form-group">
                                <input value="{{ $medal->user_id ?? old('user_id') }}" class="form-control" type="text" name="user_id" placeholder="ID пользователя">
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Медаль</label>
                                    </div>
                                    <select name="medal_id" class="custom-select">
                                        @foreach ($medals as $medal)
                                            <option value="{{ $medal->id }}">{{ $medal->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Выдать</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
