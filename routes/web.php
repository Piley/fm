<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\File;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::any('ulogin','SocialController@ulogin');

Route::get('user','UserController@index')->name('user.index');

Route::get('user/{id}', 'UserController@show')->name('user.show');

Route::get('settings', 'UserController@settings')->name('settings');;

Route::put('settings/update', 'UserController@update');

Route::get('medal','UserController@medal')->name('medal');
Route::post('medalStore','UserController@medalStore')->name('medalStore');

Route::get('policy','UserController@policy')->name('policy');

//Route::get('images/{filename}', function ($filename)
//{
//    $path = storage_path('app\users\\' . $filename);
//
//    if (!File::exists($path)) {
//        abort(404);
//    }
//
//    $file = File::get($path);
//    $type = File::mimeType($path);
//
//    $response = Response::make($file, 200);
//    $response->header("Content-Type", $type);
//
//    return $response;
//});


//Route::get('marathon','MarathonController@index');
//Route::get('marathon/{id}', 'MarathonController@show')->name('marathon.show');
Route::resource('marathon', 'MarathonController');
Route::get('marathon/{id}/join','MarathonController@join')->name('marathon.join');
Route::get('marathon/{id}/markDay','MarathonController@markDay')->name('marathon.markDay');
Route::get('marathon/{id}/control','MarathonController@control')->name('marathon.control');
Route::post('marathon/{id}/kick','MarathonController@kick')->name('marathon.kick');
Route::post('marathon_add_comment','MarathonController@addComment');
Route::get('marathon_edit_comment/{id}','MarathonController@editComment')->name('marathon_edit_comment');
Route::post('marathon_edit_comment_update/{id}','MarathonController@editCommentUpdate')->name('marathon_edit_comment_update');

//Route::resource('post', 'PostController');
//Route::post('post_add_comment','PostController@addComment');