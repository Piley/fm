<?php

namespace App;

use App\Models\Marathon;
use App\Models\Medal;
use App\Models\Post;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    const USER_RIGHTS_BANNED = -100;
    const USER_RIGHTS_USER = 0;
    const USER_RIGHTS_MODER = 50;
    const USER_RIGHTS_ADMIN = 100;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image', 'about', 'sex',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function marathons()
    {
        return $this->belongsToMany(Marathon::class, 'user_marathon');
    }

    public function activeMarathons()
    {
        return $this->belongsToMany(Marathon::class, 'user_marathon')
            ->where('status', '=', 1);
    }

    public function medals()
    {
        return $this->belongsToMany(Medal::class, 'user_medal')
            ->withPivot('created_at');
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function isAdmin()
    {
        return Auth::user()->rights == self::USER_RIGHTS_ADMIN;
    }

    public function isModer()
    {
        return Auth::user()->rights >= self::USER_RIGHTS_MODER;
    }

    public function isBanned()
    {
        return Auth::user()->rights == self::USER_RIGHTS_BANNED;
    }
}
