<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medal extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_medal')
            ->withPivot('created_at');
    }
}
