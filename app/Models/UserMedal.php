<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMedal extends Model
{
    protected $fillable = ['user_id', 'medal_id', 'source'];
    protected $table = 'user_medal';
}
