<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class Marathon extends Model
{
    const MARATHON_STATUS_PENDING = 0;
    const MARATHON_STATUS_REG = 1;
    const MARATHON_STATUS_ACTIVE = 2;
    const MARATHON_STATUS_FINISH = 3;

    public static $status_names = [
        self::MARATHON_STATUS_PENDING => 'Запись скоро начнется',
        self::MARATHON_STATUS_REG => 'Идет запись',
        self::MARATHON_STATUS_ACTIVE => 'В процессе',
        self::MARATHON_STATUS_FINISH => 'Завершен',
    ];

    protected $fillable = [
        'title', 'description', 'rules', 'tips', 'auto_kick_days', 'repeat_type', 'weekends', 'start_at', 'end_at', 'reg_start_at', 'reg_end_at', 'image',
    ];

    protected $dates = ['start_at', 'end_at', 'reg_start_at', 'reg_end_at'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_marathon')
            ->withPivot('status');
    }

    public function activeUsers()
    {
        return $this->belongsToMany(User::class, 'user_marathon')
            ->where('status', '=', 1)
            ->withPivot('status');;
    }

    public function comments()
    {
        return $this->hasMany(MarathonComment::class)
            ->orderBy('created_at', 'desc');
    }

    public function isUserMember($user_id)
    {
        $is_exist = UserMarathon::where('user_id', $user_id)
            ->where('marathon_id', $this->id)
            ->where('status', 1)
            ->exists();

        return $is_exist;
    }

    public function markDay($user_id, $time = null)
    {
        $time = isset($time) ? $time : time();
        $day = date('Y-m-d 00:00:00', $time);

        if ($this->isUserMember($user_id)) {
            if (!$this->isMarkedToday($user_id)) {
                MarathonMark::create([
                    'marathon_id' => $this->id,
                    'user_id' => $user_id,
                    'check_date' => $day,
                ]);
            }
        }
    }

    public function getUserMarks($user_id)
    {
        $marks = MarathonMark::where('user_id', $user_id)
            ->where('marathon_id', $this->id)
            ->get();

        return $marks;
    }

    public function getUserMarksForGraph($user_id)
    {
        $marks = MarathonMark::where('user_id', $user_id)
            ->where('marathon_id', $this->id)
            ->get();
        $graph = [];
        /* create grid of dates */
        $duration = $this->getDurationDays();
        $start_date = $this->start_at->format('d.m.Y');

        for ($day = 1; $day <= $duration; $day++) {
            $graph[$start_date] = ['num' => $day, 'success' => false, 'today' => false];
            $start_date = date('d.m.Y', strtotime("+1 day", strtotime($start_date)));
        }

        foreach ($marks as $mark) {
            if (array_key_exists($mark->check_date->format('d.m.Y'), $graph)) {
                $graph[$mark->check_date->format('d.m.Y')]['success'] = true;
            }
        }

        $today = date('d.m.Y', time());
        if (array_key_exists($today, $graph)) {
            $graph[$today]['today'] = true;
        }

        return $graph;
    }

    public function isMarkedToday($user_id)
    {
        $time = time();
        $from = date('Y-m-d 00:00:00', $time);
        $to = date('Y-m-d 23:59:59', $time);
        $is_exist = MarathonMark::where('user_id', $user_id)
            ->where('check_date', '>=', $from)
            ->where('check_date', '<=', $to)
            ->where('marathon_id', $this->id)
            ->exists();

        return $is_exist;
    }

    public function joinMember($user_id)
    {
        $reg = new UserMarathon();
        $reg->marathon_id = $this->id;
        $reg->user_id = $user_id;
        $reg->status = 1;
        $reg->save();
    }

    public function cancelMember($user_id)
    {
        UserMarathon::where('user_id', $user_id)
            ->update(['status' => 0]);
    }

    public function kickMember($user_id)
    {
        UserMarathon::where('user_id', $user_id)
            ->update(['status' => 2]);
    }

    public function returnMember($user_id)
    {
        UserMarathon::where('user_id', $user_id)
            ->where('status', 2)
            ->update(['status' => 1]);
    }

    public function getStatus()
    {
        $status = self::MARATHON_STATUS_PENDING;
        $now = time();
        if ($now < strtotime($this->reg_start_at)) {
            $status = self::MARATHON_STATUS_PENDING;
        } elseif ($now > strtotime($this->reg_start_at) and $now < strtotime($this->start_at)) {
            $status = self::MARATHON_STATUS_REG;
        } elseif ($now > strtotime($this->start_at) and $now < strtotime($this->end_at)) {
            $status = self::MARATHON_STATUS_ACTIVE;
        } elseif ($now > strtotime($this->end_at)) {
            $status = self::MARATHON_STATUS_FINISH;
        }
        return $status;
    }

    public function getDurationDays()
    {
        $days = Carbon::parse($this->start_at)->diffInDays(Carbon::parse($this->end_at));
        return $days;
    }

    public function getStatusName()
    {
        return self::$status_names[$this->getStatus()];
    }
}
