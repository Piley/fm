<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarathonMark extends Model
{
    protected $fillable = ['marathon_id', 'user_id', 'check_date'];
    protected $dates = ['check_date'];
}
