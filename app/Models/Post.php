<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'category_id', 'user_id', 'body',
    ];

    public function comments()
    {
        return $this->hasMany(PostComment::class)
            ->orderBy('created_at', 'desc');
    }
}
