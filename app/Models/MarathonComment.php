<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarathonComment extends Model
{
    protected $fillable = [
        'body', 'user_id', 'marathon_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
