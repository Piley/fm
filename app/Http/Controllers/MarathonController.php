<?php

namespace App\Http\Controllers;

use App\Models\Marathon;
use App\Models\MarathonComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MarathonController extends Controller
{
    public function __construct(Marathon $marathon, MarathonComment $comment)
    {
        $this->marathon = $marathon;
        $this->comment = $comment;
    }

    public function index()
    {
        $marathons = $this->marathon->latest()->where('is_deleted', '=', 0)->paginate(6);

        return view('marathon.index',
            ['marathons' => $marathons]
        );
    }

    public function show($id)
    {
        $marathon = $this->marathon::findOrFail($id);
        $comments = $marathon->comments()->latest()->paginate(10);
        $marks = false;
        if (Auth::check()){
            $marks = $marathon->getUserMarksForGraph(Auth::id());
        }

        return view('marathon.show',
            [
                'comments' => $comments,
                'marathon' => $marathon,
                'status' => $marathon->getStatus(),
                'marks' => $marks,
            ]
        );
    }

    public function create()
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }
        return view('marathon.create');
    }

    public function store(Request $request)
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'rules' => 'required',
            'auto_kick_days' => 'integer',
            'repeat_type' => 'integer',
            'weekends' => 'required|integer',
            'start_at' => 'required|date',
            'end_at' => 'required|date',
            'reg_start_at' => 'required|date',
            'reg_end_at' => 'required|date',
            'image' => 'required',
        ]);

        $data = $request->all();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $name = time() . "_marathon_image";
            $extension = $request->image->extension();
            $nameImage = "{$name}.$extension";
            $data['image'] = $nameImage;

            $upload = $request->image->storeAs('public/uploads/images/marathon', $nameImage);
            if (!$upload)
                return redirect()
                    ->back()
                    ->with('errors', ['Ошибка загрузки файла'])
                    ->withInput();
        }

        $this->marathon::create($data);

        return redirect()
            ->route('marathon.index');
    }

    public function edit($id)
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }
        if (!$marathon = $this->marathon->find($id))
            return redirect()->back();

        return view('marathon.edit', compact('marathon'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }
        if (!$marathon = $this->marathon->find($id))
            return redirect()->back();

        $data = $request->all();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $name = time() . "_marathon_image";
            $extension = $request->image->extension();
            $nameImage = "{$name}.$extension";
            $data['image'] = $nameImage;

            $upload = $request->image->storeAs('public/uploads/images/marathon', $nameImage);

            if (!$upload)
                return redirect()
                    ->back()
                    ->with('errors', ['Ошибка загрузки файла'])
                    ->withInput();
        }

        $marathon->update($data);

        return redirect()
            ->route('marathon.index');
    }

    public function destroy($id)
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }
        if (!$marathon = $this->marathon->find($id))
            return redirect()->back();

        $marathon->delete();

        return redirect()
            ->route('marathon.index');
    }

    public function addComment(Request $request)
    {
        if (!Auth::check()) return redirect('');
        $request->validate([
            'body' => 'required',
            'marathon_id' => 'required',
        ]);

        $marathon = $this->marathon::findOrFail($request->marathon_id);

        MarathonComment::create([
            'body' => $request->body,
            'user_id' => Auth::id(),
            'marathon_id' => $marathon->id
        ]);

        return redirect()->route('marathon.show', $marathon->id);
    }

    public function editComment($id)
    {
        $comment = $this->comment->find($id);
        if (!Auth::check() or !$comment) return redirect()->back();
        if (Auth::id() != $comment->user_id and !Auth::user()->isModer()) return redirect()->back();

        return view('marathon.edit_comment', compact('comment'));
    }

    public function editCommentUpdate(Request $request, $id)
    {
        $comment = $this->comment->find($request->id);
        if (!Auth::check() or !$comment) return redirect()->back();
        if (Auth::id() != $comment->user_id and !Auth::user()->isModer()) return redirect()->back();
        $request->validate([
            'body' => 'required',
        ]);
        $comment->update($request->all());

        return redirect()->route('marathon.show', $comment->marathon_id);
    }

    public function join($id)
    {
        $marathon = $this->marathon->find($id);
        if (!Auth::check() or !$marathon) return redirect()->back();

        $is_member = $marathon->isUserMember(Auth::id());

        if ($is_member) {
            $marathon->cancelMember(Auth::id());
        } else {
            $marathon->joinMember(Auth::id());
        }

        return redirect()->route('marathon.show', $marathon->id);
    }

    public function markDay($id)
    {
        $marathon = $this->marathon->find($id);
        if (!Auth::check() or !$marathon) return redirect()->back();

        $is_member = $marathon->isUserMember(Auth::id());

        if ($is_member) {
            $marathon->markDay(Auth::id());
        }

        return redirect()->route('marathon.show', $marathon->id);
    }

    public function control($id)
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }
        $marathon = $this->marathon::findOrFail($id);
        return view('marathon.control',
            [
                'marathon' => $marathon,
            ]
        );
    }

    public function kick(Request $request, $id)
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }
        $user_id = $request->user_id;
        if (!$marathon = $this->marathon->find($id) or !$user_id)
            return redirect()->back();
        $status = 'unknown';
        if ($marathon->isUserMember($user_id)) {
            $marathon->kickMember($user_id);
            $status = 'kicked';
        } else {
            $marathon->returnMember($user_id);
            $status = 'joined';
        }
        return response()->json(['status' => $status]);
    }
}
