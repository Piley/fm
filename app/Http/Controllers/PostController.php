<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostCategory;
use App\Models\PostComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function index()
    {
        $posts = $this->post->latest()->paginate();

        return view('post.index',
            ['posts' => $posts]
        );
    }

    public function show($id)
    {
        $post = $this->post::findOrFail($id);
        return view('post.show', ['post' => $post]);
    }

    public function create()
    {
        if (!Auth::check()) return redirect()->back();
        $categories = PostCategory::get();
        return view('post.create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        if (!Auth::check()) return redirect()->back();

        $request->validate([
            'title' => 'required',
            'category_id' => 'required|integer',
            'body' => 'required',
        ]);

        $data = $request->all();
        $data['user_id'] = Auth::id();

        $this->post::create($data);

        return redirect()
            ->route('post.index');
    }

    public function edit($id)
    {
        if (!$post = $this->post->find($id))
            return redirect()->back();

        if (!Auth::id() == $post->user_id and !Auth::user()->isModer()) {
            abort(404);
        }

        return view('post.edit',  ['post' => $post]);
    }

    public function update(Request $request, $id)
    {
        if (!$post = $this->post->find($id))
            return redirect()->back();

        if (!Auth::id() == $post->user_id and !Auth::user()->isModer()) {
            abort(404);
        }

        $data = $request->all();

        $post->update($data);

        return redirect()
            ->route('post.index');
    }

    public function destroy($id)
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }
        if (!$post = $this->post->find($id))
            return redirect()->back();

        $post->delete();

        return redirect()
            ->route('post.index');
    }

    public function addComment(Request $request)
    {
        if (!Auth::check()) return redirect('');
        $request->validate([
            'post_id' => 'required',
            'body' => 'required',
        ]);

        $post = $this->post::findOrFail($request->post_id);

        PostComment::create([
            'body' => $request->body,
            'user_id' => Auth::id(),
            'post_id' => $post->id
        ]);

        return redirect()->route('post.show', $post->id);
    }
}
