<?php

namespace App\Http\Controllers;

use App\Models\Medal;
use App\Models\UserMedal;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $users = $this->user->latest()->paginate();

        return view('user.index',
            ['users' => $users]
        );
    }

    public function show($id)
    {
        $user = $this->user::findOrFail($id);
        return view('user.show',
            ['user' => $user]
        );
    }

    public function settings()
    {
        if (!Auth::check()) return redirect('');

        $user = $this->user::findOrFail(Auth::user()->id);
        return view('user.settings',
            ['user' => $user]
        );
    }

    public function update(Request $request)
    {
        if (!$user = $this->user->find(Auth::user()->id))
            return redirect()->back();

        $data = $request->all();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            // Remove image if exists
            if ($user->image) {
                if (Storage::exists("users/{$user->image}"))
                    Storage::delete("users/{$user->image}");
            }

            $name = time() . "_" . $user->id;
            $extension = $request->image->extension();
            $nameImage = "{$name}.$extension";
            $data['image'] = $nameImage;

            $upload = $request->image->storeAs('public/uploads/images/user', $nameImage);
            if (!$upload)
                return redirect()
                    ->back()
                    ->with('errors', ['Ошибка загрузки файла'])
                    ->withInput();
        }

        $user->update($data);

        return redirect()
            ->route('settings')
            ->withSuccess('Данные успешно сохранены!');
    }

    public function medal()
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }
        
        $medals = Medal::get();

        return view('user.medal',
            ['medals' => $medals]
        );
    }

    public function medalStore(Request $request)
    {
        if (!Auth::user()->isModer()) {
            abort(404);
        }

        $request->validate([
            'user_id' => 'required|integer|exists:users,id',
            'medal_id' => 'required|integer',
        ]);

        UserMedal::create($request->all());
        return redirect()
            ->route('medal')
            ->withSuccess('Медаль успешно выдана!');
    }

    public function policy()
    {
        return view('user.policy');
    }
}
